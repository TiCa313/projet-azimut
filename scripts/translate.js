
const LOCALE_KEY = "i18n_locale";

document.addEventListener("DOMContentLoaded", function (event) {
    let stored_locale = localStorage.getItem(LOCALE_KEY);
    let locale = stored_locale ? stored_locale : "fr";

    var requete = new XMLHttpRequest();
    requete.open("GET", `./messages/${locale}.json`, false);
    requete.send();
    if (requete.readyState == 4 && requete.status == 200) {
        i18n_data = JSON.parse(requete.responseText);
        for (property in i18n_data) {
            let elt = document.getElementById(property)
            if (elt) {
                elt.innerHTML = i18n_data[property];
            }
        }
    }
})

function changeLanguage(language) {
    localStorage.setItem(LOCALE_KEY, language);
    window.location.reload();
}